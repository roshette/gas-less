import { ValueOf } from '@/core/types';

export const THEMES = {
  LIGHT: 'light' as const,
  DARK: 'dark' as const,
};

export type ThemeType = ValueOf<typeof THEMES>;

export const DEFAULT_THEME = THEMES.LIGHT;

export const COLOR_SCHEME = 'color-theme';

const useTheme = () => {
  const setTheme = (theme: ThemeType) => {
    if (theme === THEMES.DARK || (!(COLOR_SCHEME in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
      document.documentElement.classList.add(THEMES.DARK);
    } else {
      document.documentElement.classList.remove(THEMES.DARK);
    }
  };

  const store = (theme: ThemeType): ThemeType => {
    localStorage.setItem(COLOR_SCHEME, theme);

    setTheme(theme);

    return theme;
  };

  const restore = (): ThemeType => {
    const theme = localStorage.getItem(COLOR_SCHEME) as ThemeType | undefined ?? DEFAULT_THEME;

    setTheme(theme);

    return theme;
  };

  return {
    store,
    restore,
  };
};

export default useTheme;
