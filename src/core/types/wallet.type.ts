export interface Wallet {
  internalAddress: string;
  externalAddress: string;
}
