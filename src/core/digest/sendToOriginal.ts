import { ethers } from 'ethers';
import { getDomainSeparator } from './domainSeparator';
import { MESSAGE_TYPEHASH } from './const';

export const SEND_ORIGINAL_TYPEHASH = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('SendOriginal(address owner,address to,uint256 value,address commissionTo,uint256 commissionValue,uint256 nonce,uint256 deadline)'));

export const getSendOriginalDigest = (tokenName: string, tokenAddress: string, sendOriginal: any, nonce: any, deadline: any, chainID: number) => {
  const DOMAIN_SEPARATOR = getDomainSeparator(tokenName, tokenAddress, chainID);

  return ethers.utils.keccak256(ethers.utils.solidityPack(['bytes1', 'bytes1', 'bytes32', 'bytes32'], [
    '0x19',
    '0x01',
    DOMAIN_SEPARATOR,
    ethers.utils.keccak256(
      ethers.utils.defaultAbiCoder.encode(
        ['bytes32', 'bytes32', 'address', 'address', 'uint256', 'uint256', 'uint256', 'uint256'],
        [
          MESSAGE_TYPEHASH,
          SEND_ORIGINAL_TYPEHASH,
          sendOriginal.from,
          sendOriginal.to,
          sendOriginal.value,
          sendOriginal.commissionValue,
          nonce,
          deadline,
        ],
      ),
    ),
  ]));
};
