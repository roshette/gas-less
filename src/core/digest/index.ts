import * as send from './sendDigest';
import * as sendFromOriginal from './sendFromOriginal';
import * as sendToOriginal from './sendToOriginal';

export default {
  ...send,
  ...sendFromOriginal,
  ...sendToOriginal,
};
