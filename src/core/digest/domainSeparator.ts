import { ethers } from 'ethers';

export const getDomainSeparator = (name: string, tokenAddress: string, chainID: number) => ethers.utils.keccak256(
  ethers.utils.defaultAbiCoder.encode(
    ['bytes32', 'bytes32', 'bytes32', 'uint256', 'address'],
    [
      ethers.utils.keccak256(ethers.utils.toUtf8Bytes('EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)')),
      ethers.utils.keccak256(ethers.utils.toUtf8Bytes(name)),
      ethers.utils.keccak256(ethers.utils.toUtf8Bytes('1')),
      chainID,
      tokenAddress,
    ],
  ),
);
