import { ethers } from 'ethers';

export const MESSAGE_TYPEHASH = ethers.utils.keccak256(
  ethers.utils.toUtf8Bytes(
    'Message(bytes32 methodTypehash,address from,address to,uint256 value,uint256 commissionValue,uint256 nonces,uint256 deadline)',
  ),
);
