import { ethers } from 'ethers';
import { splitSignature } from 'ethers/lib/utils.js';
import { Contract } from '@ethersproject/contracts/src.ts';
import { ExternalProvider, Web3Provider } from '@ethersproject/providers/src.ts/web3-provider';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { Wallet } from '@/core/types/wallet.type';
import unionAbi from './abi/union-abi.json';
import usdtAbi from './abi/usdt-abi.json';
import digest from './digest';
import { Sign } from 'crypto';

const UNION_CONTRACT_ADDRESS: string = import.meta.env.VITE_UNION_CONTRACT_ADDRESS;
const USDT_CONTRACT_ADDRESS: string = import.meta.env.VITE_USDT_CONTRACT_ADDRESS;

const DIGEST_TYPEHASH = {
  SEND_FROM_ORIGINAL: digest.SEND_FROM_ORIGINAL_TYPEHASH,
  SEND_ORIGINAL: digest.SEND_ORIGINAL_TYPEHASH,
  SEND: digest.SEND_TYPEHASH,
};

export type DigestTypehash = keyof typeof DIGEST_TYPEHASH;

const TYPES = {
  Message: [
    { name: 'methodTypehash', type: 'bytes32' },
    { name: 'from', type: 'address' },
    { name: 'to', type: 'address' },
    { name: 'value', type: 'uint256' },
    { name: 'commissionValue', type: 'uint256' },
    { name: 'nonces', type: 'uint256' },
    { name: 'deadline', type: 'uint256' },
  ],
};

const SMART_CONTRACT_VERSION = '1';

export interface SignCreatorAddresses {
  getAddresses: () => Wallet
}

export class SignCreator implements SignCreatorAddresses {
  provider: Web3Provider;

  signer?: JsonRpcSigner;

  signerAddress?: string;

  providerChainId?: number;

  usdtContract?: Contract;

  unionContract?: Contract;

  unionTokenName?: string;

  UnionBalance?: string;

  USDTBalance?: string;

  totalBalance?: string;

  constructor(provider: ExternalProvider) {
    this.provider = new ethers.providers.Web3Provider(provider, 'any');
  }

  static prepareData(amount: string, commission = '20') {
    return {
      amountBigNumber: ethers.utils.parseUnits(amount, 16),
      commissionBigNumber: ethers.utils.parseUnits(commission, 16),
    };
  }

  private getDomain() {
    return {
      name: this.unionTokenName,
      version: SMART_CONTRACT_VERSION,
      chainId: this.providerChainId,
      verifyingContract: UNION_CONTRACT_ADDRESS,
    };
  }

  private async setProviderChainId() {
    const { chainId } = await this.provider.getNetwork();

    this.providerChainId = chainId;
  }

  private async setSigner() {
    this.signer = await this.provider.getSigner();
    this.signerAddress = await this.signer.getAddress();
  }

  private async setContracts() {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.unionContract = new ethers.Contract(
      UNION_CONTRACT_ADDRESS,
      unionAbi,
      this.signer,
    );
    this.unionTokenName = await this.unionContract?.name();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.usdtContract = new ethers.Contract(
      USDT_CONTRACT_ADDRESS,
      usdtAbi,
      this.signer,
    );
  }

  public async init() {
    await this.provider.send('eth_requestAccounts', []);
    await this.setProviderChainId();
    await this.setSigner();
    await this.setContracts();
    await this.setBalance();
    console.log('inited!');
  }

  public async createSign(to: string, amount: string, transferType: DigestTypehash) {
    const { amountBigNumber, commissionBigNumber } = SignCreator.prepareData(amount);

    if (!this.unionContract) {
      throw new Error('unionContract is not found!');
    }

    const nonce = await this.unionContract.nonces(this.signerAddress);
    const deadline = ethers.constants.MaxUint256;
    const domain = this.getDomain();
    const value = {
      methodTypehash: DIGEST_TYPEHASH[transferType],
      from: this.signerAddress,
      to,
      value: amountBigNumber,
      commissionValue: commissionBigNumber,
      nonces: nonce,
      deadline,
    };

    if (!this.signer) {
      throw new Error('Signer is not found!');
    }

    const signature = await this.signer._signTypedData(domain, TYPES, value);
    const { v, r, s } = splitSignature(signature);

    return {
      ...value,
      v,
      r,
      s,
    };
  }

  public async approveUSDTTransfer(amount: string) {
    const transferAmount = ethers.utils.parseUnits(amount, 16);

    if (!this.usdtContract) {
      throw new Error('USDT contract wasn\'t initialize');
    }
    const approve = await this.usdtContract.approve(UNION_CONTRACT_ADDRESS, transferAmount);

    return approve.wait();
  }

  private async setBalance() {
    const USDTBalance = await this.usdtContract?.balanceOf(this.signerAddress);
    const UnionBalance = await this.unionContract?.balanceOf(this.signerAddress);
    const sumBalance = USDTBalance.add(UnionBalance);

    this.USDTBalance = ethers.utils.formatEther(USDTBalance);
    this.UnionBalance = ethers.utils.formatEther(UnionBalance);
    this.totalBalance = ethers.utils.formatEther(sumBalance);
  }

  public getAddresses(): Wallet {
    if (!this.signerAddress) {
      throw new Error('Empty signer address');
    }

    return {
      internalAddress: `FF${this.signerAddress?.slice(2)}`,
      externalAddress: this.signerAddress,
    };
  }

  // Когда USDT to internal
  // Когда внутренний перевод
  // Когда из внутреннего на внешний
  // Когда перевод комбинированный перевод (USDT & UnionToken) to internal
  public static getTransferType(address: string, amount: string) {
    const isInternal = address.substring(0, 2) === 'FF';

    return isInternal;
  }

  public static isValidAddress(address: string) {
    return ethers.utils.isAddress(address);
  }
}
