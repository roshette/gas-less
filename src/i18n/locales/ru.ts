export default {
  next: 'Далее',
  transfer: 'Перевод',
  wallet: 'Кошелек',
  internalAddress: 'Внутренний адрес',
  externalAddress: 'Внешний адрес',
  transferTokens: 'Перевести токены',
  recipient: 'Получатель',
  amount: 'Сумма',
  commission: 'Комиссия',
  total: 'Итог',
  enterAddress: 'Введите адрес',
  enterTokenAmount: 'Введите количество токенов',
  metaMaskNotFound: 'Расширение МетаМаск не обнаружен! Пожалуйста, установите расширение для вашего браузера!',
  installMetaMask: 'Установите МетаМаск',
};
