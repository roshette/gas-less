import { createI18n } from 'vue-i18n';
import { ValueOf } from '@/core/types';
import en from './locales/en';
import ru from './locales/ru';

export const LANGS = {
  en: 'English' as const,
  ru: 'Русский' as const,
};

export type Langs = keyof typeof LANGS;

export type LangTitle = ValueOf<typeof LANGS>

export type Locale = {
  title: LangTitle,
  value: Langs,
}

export const LOCALES = Object.entries(LANGS).map(([value, title]): Locale => ({
  title,
  value: value as Langs,
}));

const messages = {
  en,
  ru,
};

const i18n = createI18n({
  legacy: false,
  locale: 'ru',
  fallbackLocale: 'en',
  messages,
});

export const useSettingI18n = () => {
  const DEFAULT_LANG = 'en';

  const setLang = (lang: Langs): Langs => {
    i18n.global.locale.value = lang;

    return lang;
  };

  const store = (lang: Langs): Langs => {
    setLang(lang);

    localStorage.setItem('lang', lang);

    return lang;
  };

  const setDefaultLang = (): Langs => store(DEFAULT_LANG);

  const restore = (): Langs => {
    const lang = localStorage.getItem('lang') as Langs | undefined;

    return lang ? setLang(lang) : setDefaultLang();
  };

  return {
    LOCALES,
    LANGS,
    store,
    restore,
  };
};

export default i18n;
