import PreviewPage from '../components/pages/Preview.vue';
import HomePage from '../components/pages/HomePage.vue';
import NotFoundPage from '../components/pages/NotFoundPage.vue';
import MetamaskNotFoundPage from '../components/pages/MetamaskNotFoundPage.vue';

export const HOME_PAGE = {
  name: 'home-page',
  path: '/',
  component: HomePage,
};

export const NOT_FOUND = {
  name: 'not-found-page',
  path: '/:pathMatch(.*)*',
  component: NotFoundPage,
};

export const METAMASK_NOT_FOUND = {
  name: 'metamask-not-found-page',
  path: '/metamask',
  component: MetamaskNotFoundPage,
};

export const PREVIEW = {
  name: 'preview',
  path: '/preview',
  component: PreviewPage,
};

const routes = [
  HOME_PAGE,
  NOT_FOUND,
  METAMASK_NOT_FOUND,
  PREVIEW,
];

export default routes;
