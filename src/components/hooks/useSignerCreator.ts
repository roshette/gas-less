import { onMounted, Ref, ref } from 'vue';
import { SignCreator } from '@/core';
import { useRouter } from 'vue-router';
import { METAMASK_NOT_FOUND } from '@/router/routes';

const useSignerCreator = () => {
  const loading = ref(true);
  const router = useRouter();
  const signCreator: Ref<SignCreator | undefined> = ref();

  onMounted(async () => {
    if (!window?.ethereum) {
      await router.push({ name: METAMASK_NOT_FOUND.name });

      throw Error('metamask is not found');
    }

    signCreator.value = new SignCreator(window.ethereum);
    await signCreator.value.init();
    loading.value = false;
  });

  return {
    loading,
    signCreator,
  };
};

export default useSignerCreator;
