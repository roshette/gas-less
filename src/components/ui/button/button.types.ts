import { ValueOf } from '@/core/types';

export const TYPE_STYLES = {
  DEFAULT: 'default' as const,
  OUTLINE: 'outline' as const,
  ICON: 'icon' as const,
};

export type TypeStyle = ValueOf<typeof TYPE_STYLES>
