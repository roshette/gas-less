import type { InjectionKey } from 'vue';

export const SIGNER_CREATOR_SYMBOL = Symbol('SignerCreator') as InjectionKey<string>;
export const SIGNER_CREATOR_LOADER_SYMBOL = Symbol('SignerCreatorLoader') as InjectionKey<string>;
