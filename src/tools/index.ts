export const buildPath = (...args: string[]) => args
  .map((part, i) => {
    if (i === 0) {
      return part.trim().replace(/[/]*$/g, '');
    }

    return part.trim().replace(/(^[/]*|[/]*$)/g, '');
  })
  .filter((x) => x.length)
  .join('/');

export const stringifyParams = (payload: Record<string, string | number | boolean>) => {
  const params = new URLSearchParams();

  Object.keys(payload).forEach((key) => {
    params.append(key, String(payload[key]));
  });

  return params;
};
