export type TransactionPayload = {
  r: string;
  v: number;
  s: string;
  from: string;
  to: string;
  amount: string;
  commission_amount: string;
  transfer_type: number;
}
