import { AxiosResponse } from 'axios';
import { buildPath, stringifyParams } from '@/tools';
import { TransactionPayload } from './transfer.types';
import customAxios from '../axios.config';

const API_VERSION_1 = 'v1';

export const sendTransaction = async (payload: TransactionPayload): Promise<AxiosResponse<any>> => {
  const url = buildPath(API_VERSION_1, 'transaction');

  return customAxios.post<any>(url, stringifyParams(payload));
};
